<?php
require 'templates/header.php';
?>
			<!-- start banner Area -->
			<section class="banner-area relative about-banner" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								О нас
							</h1>	
							<p class="text-white link-nav"><a href="index.php">Главная</a>  <span class="lnr lnr-arrow-right"></span>  <a href="about.php">О нас</a></p>
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->	

			<!-- Start feature Area -->
			<section class="feature-area pb-120">
				<div class="container">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="single-feature">
                                <div class="title">
                                    <h4>Учитесь онлайн</h4>
                                </div>
                                <div class="desc-wrap">
                                    <p>
                                        Использование Интернета в обущении становится все более распространенным благодаря быстрому развитию технологий.
                                    </p>
                                    <a href="#">Начать</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="single-feature">
                                <div class="title">
                                    <h4>Одна из лучших платформ</h4>
                                </div>
                                <div class="desc-wrap">
                                    <p>
                                        Получите первый опыт программирования с нами. Изучите материалы в самые короткие сроки!
                                    </p>
                                    <a href="#">Начать</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="single-feature">
                                <div class="title">
                                    <h4>Огромный выбор материалов</h4>
                                </div>
                                <div class="desc-wrap">
                                    <p>
                                        Если вы, как и многие из нас, серьезный фанатик программирования, вы, вероятно, найдете для себя все что нужно.
                                    </p>
                                    <a href="#">Начать</a>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>	
			</section>
			<!-- End feature Area -->		

			<!-- Start info Area -->
			<section class="info-area pb-120">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-lg-6 no-padding info-area-left">
							<img class="img-fluid" src="img/about-img.jpg" alt="">
						</div>
						<div class="col-lg-6 info-area-right">
							<h1>Кто мы такие?</h1>
							<p>Мы выбрали одно направление — web программирование — и сделали самые подробные тренажёры о веб-технологиях. </p>
							<br>
							<p>
                                С самого начала мы учим работать с живым кодом и самостоятельно решать задачи, приближённые к реальным.
							</p>
						</div>
					</div>
				</div>	
			</section>
			<!-- End info Area -->	

			<!-- Start course-mission Area -->
			<section class="course-mission-area pb-120">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-70 col-lg-8">
							<div class="title text-center">
								<h1 class="mb-10">Цели которых вы достигнете вместе с нами</h1>
								<p>Кто безумно любит программирование</p>
							</div>
						</div>
					</div>							
                    <div class="row">
                        <div class="col-md-6 accordion-left">

                            <!-- accordion 2 start-->
                            <dl class="accordion">
                                <dt>
                                    <a href="">Теория</a>
                                </dt>
                                <dd>
                                    Мы дадим вам полные теоритические знания и время для их закрепления
                                </dd>
                                <dt>
                                    <a href="">Практика</a>
                                </dt>
                                <dd>
                                    Уже в процессе обучения вы сможете проверять свои знания на практических заданиях
                                </dd>
                                <dt>
                                    <a href="">Вакансии</a>
                                </dt>
                                <dd>
                                    После прохождения наших курсов вам будет хватать знаний и вы можете выйти на стажировку в крупную компанию.
                                </dd>
                            </dl>
                            <!-- accordion 2 end-->
                        </div>
                        <div class="col-md-6 video-right justify-content-center align-items-center d-flex relative">
                        	<div class="overlay overlay-bg"></div>
							<a class="play-btn" href="https://www.youtube.com/watch?v=ARA0AxrnHdM"><img class="img-fluid mx-auto" src="img/play.png" alt=""></a>
                        </div>
                    </div>
				</div>	
			</section>
			<!-- End course-mission Area -->
					

			<!-- Start search-course Area -->
<section class="search-course-area relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-lg-6 col-md-6 search-course-left">
                <h1 class="text-white">
                    Есть вопросы? <br>
                    Напиши нам!
                </h1>
                <p>
                    Всегда открыты для ваших предложений и вопросов!
                </p>
                <div class="row details-content">
                    <div class="col single-detials">
                        <span class="lnr lnr-graduation-hat"></span>
                        <a href="#"><h4>Опытные преподаватели</h4></a>
                        <p>
                            Использование Интернета в обучении становится все более распространенным благодаря быстрому развитию технологий и возможностей.
                        </p>
                    </div>
                    <div class="col single-detials">
                        <span class="lnr lnr-license"></span>
                        <a href="#"><h4>Сертификация</h4></a>
                        <p>
                            Выдадим сертификат за прохождения наших курсов!
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 search-course-right section-gap">
                <form class="form-wrap" action="#">
                    <h4 class="text-white pb-20 text-center mb-30">Найдите ответы на вопросы</h4>
                    <input type="text" class="form-control" name="name" placeholder="Ваше имя" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ваше имя'" >
                    <input type="phone" class="form-control" name="phone" placeholder="Ваш номер телефона" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ваш номер телефона'" >
                    <input type="email" class="form-control" name="email" placeholder="Ваш Email адрес" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ваш Email адрес'" >
                    <div class="form-select" id="service-select">
                        <select>
                            <option datd-display="">Выберите тему</option>
                            <option value="1">Вопрос</option>
                            <option value="2">Предложение</option>
                        </select>
                    </div>
                    <br>
                    <textarea type="text" class="form-control" name="email" placeholder="Ваше сообщение" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ваше сообщение'" ></textarea>
                    <button class="primary-btn text-uppercase">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</section>
			<!-- End search-course Area -->
			<!-- Start cta-two Area -->
			<section class="cta-two-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 cta-left">
                <h1>Еще не удовлетворены нашим проектом?</h1>
            </div>
            <div class="col-lg-4 cta-right">
                <a class="primary-btn wh" href="#">Посмотрите наш блог</a>
            </div>
        </div>
    </div>
</section>
			<!-- End cta-two Area -->						    			

			<!-- start footer Area -->		
			<footer class="footer-area section-gap">
				<div class="container">
					<div class="row">
						<div class="col-lg-2 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h4>Top Products</h4>
								<ul>
									<li><a href="#">Managed Website</a></li>
									<li><a href="#">Manage Reputation</a></li>
									<li><a href="#">Power Tools</a></li>
									<li><a href="#">Marketing Service</a></li>
								</ul>								
							</div>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h4>Quick links</h4>
								<ul>
									<li><a href="#">Jobs</a></li>
									<li><a href="#">Brand Assets</a></li>
									<li><a href="#">Investor Relations</a></li>
									<li><a href="#">Terms of Service</a></li>
								</ul>								
							</div>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h4>Features</h4>
								<ul>
									<li><a href="#">Jobs</a></li>
									<li><a href="#">Brand Assets</a></li>
									<li><a href="#">Investor Relations</a></li>
									<li><a href="#">Terms of Service</a></li>
								</ul>								
							</div>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h4>Resources</h4>
								<ul>
									<li><a href="#">Guides</a></li>
									<li><a href="#">Research</a></li>
									<li><a href="#">Experts</a></li>
									<li><a href="#">Agencies</a></li>
								</ul>								
							</div>
						</div>																		
						<div class="col-lg-4  col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h4>Newsletter</h4>
								<p>Stay update with our latest</p>
								<div class="" id="mc_embed_signup">
									 <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get">
									  <div class="input-group">
									    <input type="text" class="form-control" name="EMAIL" placeholder="Enter Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email Address '" required="" type="email">
									    <div class="input-group-btn">
									      <button class="btn btn-default" type="submit">
									        <span class="lnr lnr-arrow-right"></span>
									      </button>    
									    </div>
									    	<div class="info"></div>  
									  </div>
									</form> 
								</div>
							</div>
						</div>											
					</div>
					<div class="footer-bottom row align-items-center justify-content-between">
						<p class="footer-text m-0 col-lg-6 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
						<div class="col-lg-6 col-sm-12 footer-social">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-dribbble"></i></a>
							<a href="#"><i class="fa fa-behance"></i></a>
						</div>
					</div>						
				</div>
			</footer>	
			<!-- End footer Area -->	


			<script src="js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="js/vendor/bootstrap.min.js"></script>			
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="js/easing.min.js"></script>			
			<script src="js/hoverIntent.js"></script>
			<script src="js/superfish.min.js"></script>	
			<script src="js/jquery.ajaxchimp.min.js"></script>
			<script src="js/jquery.magnific-popup.min.js"></script>	
    		<script src="js/jquery.tabs.min.js"></script>						
			<script src="js/jquery.nice-select.min.js"></script>	
			<script src="js/owl.carousel.min.js"></script>									
			<script src="js/mail-script.js"></script>	
			<script src="js/main.js"></script>	
		</body>
	</html>