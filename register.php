<?php
require "function.php";
if (!empty($_POST)) {
    $errors = [];
    $username = isset($_POST['username']) ? trim($_POST['username']) : '';
    $password = isset($_POST['password']) ? trim($_POST['password']) : '';
    $email = isset($_POST['email']) ? trim($_POST['email']) : '';
    if (!$username) {
        $errors[] = "Введите имя пользователя";
    }
    elseif (isUniqueUsername($username)) {
        $errors[] = "Такой логин уже зарегистрирован";
    }
    if (!$password) {
        $errors[] = "Введите пароль";
    }
    if (!$email) {
        $errors[] = "Введите email";
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = "Введите корректный email!";
    } elseif (isUniqueEmail($email)) {
        $errors[] = "Такой email уже зарегистрирован";
    }
    if (empty($errors)) {
        $success = registerUser($username, $password, $email);
        header('Location: auth.php');
    }
}
require 'templates/auth_head.php';
?>
<body>
<div class="container">
    <div class="row">

        <div class="col-md-offset-3 col-md-6">
            <form class="form-horizontal" method="post">
                <span class="heading">РЕГИСТРАЦИЯ</span>
                <div class="form-group">
                    <input type="text" class="form-control" id="inputEmail" placeholder="Username" name="username">
                    <i class="fa fa-user"></i>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="inputEmail" placeholder="E-mail" name="email">
                    <i class="fa fa-user"></i>
                </div>
                <div class="form-group help">
                    <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password">
                    <i class="fa fa-lock"></i>
                </div>
                <div class="form-group">
                    <div class="main-checkbox">
                        <input type="checkbox" value="none" id="checkbox1" name="check"/>
                        <label for="checkbox1"></label>
                    </div>
                    <span class="text">Запомнить</span>
                    <button type="submit" class="btn btn-default">РЕГИСТРАЦИЯ</button>
                </div>
            </form>
            <?php
            if (isset($success)) {
                echo "<p align='center' style='color:black'>Регистрация прошла успешно!</p>";
            }
            require "errors.php";
            ?>
        </div>
    </div>
</div>
</body>
</html>