<?php
require 'function.php';
if (!empty($_POST)) {
    $username = isset($_POST['username']) ? $_POST['username'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';
    $user = loginUser($username, $password);
    $errors = [];
    if (!empty($user)) {
        $_SESSION['id'] = $user['id'];
        $_SESSION['name'] = $user['name'];
        $_SESSION['email'] = $user['email'];
        header('Location: index.php');
    } else {
        $errors[] = "Неверный логин или пароль!";
    }
}
require 'templates/auth_head.php';
?>
<body>
<div class="container">
    <div class="row">

        <div class="col-md-offset-3 col-md-6">
            <form class="form-horizontal" method="post">
                <span class="heading">АВТОРИЗАЦИЯ</span>
                <div class="form-group">
                    <input type="text" class="form-control" id="inputEmail" placeholder="Username" name="username">
                    <i class="fa fa-user"></i>
                </div>
                <div class="form-group help">
                    <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password">
                    <i class="fa fa-lock"></i>
                </div>
                <div class="form-group">
                    <div class="main-checkbox">
                        <input type="checkbox" value="none" id="checkbox1" name="check"/>
                        <label for="checkbox1"></label>
                    </div>
                    <span class="text">Запомнить</span>
                    <button type="submit" class="btn btn-default">ВХОД</button>
                </div>
            </form>
            <?php
            require "errors.php";
            ?>
        </div>
    </div>
</div>
</body>
</html>