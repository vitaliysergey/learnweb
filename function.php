<?php
require 'config.php';

function getPdo()
{
    static $pdo;
    if ($pdo === null) {
        $pdo = new PDO(PDO_DSN, PDO_USER, PDO_PASS);
    }
    return $pdo;
}

function registerUser($username, $password, $email)
{
    $pdo = getPdo();
    $sql = "INSERT INTO  `users` (`id` ,`name` ,`password`, `email`) VALUES (NULL ,  :name,  :password, :email);";
    $result = $pdo->prepare($sql);
    $result->bindvalue(':name', $username);
    $result->bindvalue(':email', $email);
    $result->bindvalue(':password', password_hash($password, PASSWORD_DEFAULT));
    return $result->execute();
}

function loginUser($username, $password)
{
    if (empty($username) || empty($password)) {
        return false;
    }
    $pdo = getPdo();
    $sql = "SELECT * FROM users WHERE `name`=:user_name";
    $sth = $pdo->prepare($sql);
    $sth->execute(['user_name' => $username]);
    $user = $sth->fetch(PDO::FETCH_ASSOC);
    if (!empty($user)) {
        if (password_verify($password, $user['password'])) {
            return $user;
        }
    }
    return false;
}
function isUniqueEmail($email)
{
    $sql = "SELECT count(*) FROM `users` WHERE user_email = :email";
    $result = getPdo()->prepare($sql);
    $result->execute(['email' => $email]);
    return (bool)$result->fetchColumn();
}

function isUniqueUsername($username)
{
    $sql = "SELECT count(*) FROM `users` WHERE username = :username";
    $result = getPdo()->prepare($sql);
    $result->execute(['username' => $username]);
    return (bool)$result->fetchColumn();
}

function get_kurs() {
    $pdo = getPdo();
    $query = $pdo->query('SELECT * FROM `kurs`');
    return $query->fetchAll(PDO::FETCH_OBJ);
}

function get_kurs_by_id($id) {
    $pdo = getPdo();
    $sql = "SELECT * FROM `kurs` WHERE `id` = :id";
    $sth = $pdo->prepare($sql);
    $sth->execute(['id' => $id]);
    return $sth->fetch(PDO::FETCH_OBJ);
}

function get_variable_kurs($id_kurs, $fetch_style = PDO::FETCH_OBJ) {
    $sql = "SELECT * FROM `kurs_variable` WHERE `id_kurs` = :id_kurs";
    $result = getPdo()->prepare($sql);
    $result->execute(['id_kurs' => $id_kurs]);
    return $result->fetchAll($fetch_style);
}
function get_variable_kurs_by_id($id, $fetch_style = PDO::FETCH_OBJ) {
    $sql = "SELECT * FROM `kurs_variable` WHERE `id` = :id";
    $result = getPdo()->prepare($sql);
    $result->execute(['id' => $id]);
    return $result->fetch($fetch_style);
}